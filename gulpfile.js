"use strict";

// Load plugins
const gulp = require("gulp");
const autoprefixer = require("autoprefixer");
const browsersync = require("browser-sync").create();
// const cp = require("child_process");
const cssnano = require("cssnano");
const del = require("del");
const imagemin = require("gulp-imagemin");
const newer = require("gulp-newer");
const plumber = require("gulp-plumber");
const postcss = require("gulp-postcss");
const rename = require("gulp-rename");
const sass = require("gulp-sass");
const php = require('gulp-connect-php');

// BrowserSync
function browserSync(done) {
  browsersync.init({
    baseDir: "./www",
    proxy:"localhost:8010",
    open:true,
    notify:false    
  });
  done();
}

// BrowserSync Reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}

// Clean assets
function clean() {
  return del(["./www/dist/"]);
}

// Optimize Images
function images() {
  return gulp
    .src("./www/assets/img/**/*")
    .pipe(newer("./www/dist/img"))
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.jpegtran({ progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({
          plugins: [
            {
              removeViewBox: false,
              collapseGroups: true
            }
          ]
        })
      ])
    )
    .pipe(gulp.dest("./www/dist/img"));
}

// CSS task
function css() {
  return gulp
    .src("./www/assets/scss/**/*.scss")
    .pipe(plumber())
    .pipe(sass({ outputStyle: "expanded" }))
    .pipe(gulp.dest("./www/dist/css/"))
    .pipe(rename({ suffix: ".min" }))
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(gulp.dest("./www/dist/css/"))
    .pipe(browsersync.stream());
}

// Transpile, concatenate and minify scripts
function scripts() {
  return (
    gulp
      .src(["./www/assets/js/**/*"])
      .pipe(plumber())
      .pipe(gulp.dest("./www/dist/js/"))
      .pipe(browsersync.stream())
  );
}

// PHP server
function server(done) {
    php.server({base:'./www', port:8010, keepalive:true});
    done();
}

// Watch files
function watchFiles() {
  gulp.watch("./www/assets/scss/**/*", css);
  gulp.watch("./www/assets/js/**/*", gulp.series(scripts));
  gulp.watch("./**/*.php", gulp.series(browserSyncReload));
  gulp.watch("./www/assets/img/**/*", images);
}

// define complex tasks
const js = gulp.series(scripts);
const build = gulp.series(clean, gulp.parallel(css, images, js));
const watch = gulp.parallel(server, watchFiles, browserSync);

// export tasks
exports.server = server;
exports.images = images;
exports.css = css;
exports.js = js;
exports.clean = clean;
exports.build = build;
exports.watch = watch;
exports.default = watch;